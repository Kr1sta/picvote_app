module PostsHelper

  def voted_on?(post)
    return unless current_user
    return 'voted_on' if current_user.votes.where(post_id: post.id).first.present?
  end

  def voted_class(post, vote_type)
    return unless current_user
    vote = current_user.votes.where(post_id: post.id).first


    if vote.present?
      if vote.upvote? && vote_type == :upvote
        ' clicked_btn'
      elsif !vote.upvote? && vote_type == :downvote
        ' clicked_btn'
      end
    end
  end

end
