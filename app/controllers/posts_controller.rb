class PostsController < ApplicationController

  def index
    @posts = Post.all.sort_by{ |post| post.total_votes }.reverse
  end

end
