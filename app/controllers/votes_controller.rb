class VotesController < ApplicationController
  before_action :logged_in_user, only: [:create] 

  def create
    @post = Post.find(vote_params[:post_id])
    @vote = current_user.votes.find_or_initialize_by(post_id: @post.id)
    if @vote.upvote.to_s == vote_params[:upvote]
      if @vote.destroy
        @destroy = true
      end
    else
      @vote.upvote = vote_params[:upvote]
      if @vote.save
        @success = true
      end
    end
  end

  private
  
  def vote_params
    params.require(:vote).permit(:post_id, :upvote)
  end
  
end
