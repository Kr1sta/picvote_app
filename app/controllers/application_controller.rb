class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  helper_method :current_user

  private 

  def logged_in_user
    unless user_signed_in?
      flash[:danger] = "Must be logged in to vote!"
      redirect_to new_user_session_path
    end
  end

end
