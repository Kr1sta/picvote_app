class Post < ApplicationRecord
  has_many :votes
  has_many :upvoted_users, through: :votes, source: :user

  def upvotes
    votes.where(upvote: true).count
  end

  def downvotes
    votes.where(upvote: false).count
  end

  def total_votes
    upvotes - downvotes
  end

end
