class User < ApplicationRecord
has_many :votes
has_many :upvoted_posts, through: :votes, source: :post

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  # before_save { self.name = name.downcase }
  # validates :name, presence: true, length: { maximum: 55 }, uniqueness: { case_sensitive: false }
  validates :password, presence: true, length: { minimum: 6 }
end
