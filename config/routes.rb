Rails.application.routes.draw do
  devise_for :users, controllers: {
    registrations: 'users/registrations'
  }
  root 'posts#index'
  resources :users
  resources :posts, only: [:index]
  resources :votes, only: [:create]
end
