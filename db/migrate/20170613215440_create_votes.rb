class CreateVotes < ActiveRecord::Migration[5.0]
  def change
    create_table :votes do |t|
      t.references :post, foreign_key: true
      t.boolean :upvote

      t.timestamps
    end
  end
end
